package com.epam;

import java.util.Comparator;
/**
 * Represents an comparator price of holidays.
 *
 * @author Ferbey Daria
 * @version 1.6
 * @since 2019-11-10
 */
public class HolidaysPriceComparator implements Comparator<Holidays> {
    /**
     * Compare prices of holidays.
     * @param o1 first holiday
     * @param o2 second holiday
     * @return compare prices of holidays
     */
    public int compare(Holidays o1, Holidays o2) {
        if (o1.getPrice()==o2.getPrice())
            return 0;
        return o1.getPrice()>o2.getPrice()?1:-1;
    }
}

package com.epam;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Represents an logicla of holidays.
 *
 * @author Ferbey Daria
 * @version 1.6
 * @since 2019-11-10
 */
public class BisnessLogical {
    private List<Holidays> holidays;

    /**
     * Filling arrays
     */
    public BisnessLogical() {
        holidays = new ArrayList<Holidays>();
        holidays.add(new Holidays("Soup Bubble", true, 50, 1));
        holidays.add(new Holidays("Batut", false, 120, 0.5));
        holidays.add(new Holidays("Animation", true, 200, 2));
    }

    /**
     * Print all information of holidays.
     *
     * @param holidays a list of holiday
     */
    public void showAll(List<Holidays> holidays) {
        Iterator<Holidays> iterator = holidays.iterator();
        while (iterator.hasNext()) {
            Holidays next = iterator.next();
            System.out.println("Name : " + next.getNameHoliday());
            if (next.isInFlat()) {
                System.out.println("Place : In flatt");
            } else {
                System.out.println("Place : Outdoor");
            }
            System.out.println("Price : " + next.getPrice());
            System.out.println("Duration : " + next.getDuration());
        }
    }

    /**
     * Gets holidays.
     *
     * @return holidays
     */
    public List<Holidays> getHolidays() {
        return holidays;
    }

    /**
     * Sort holidays by price.
     *
     * @return holidays sorted by price
     */
    public List<Holidays> sortByPrice() {
        return holidays.stream().sorted(new HolidaysPriceComparator()).collect(Collectors.toList());
    }

    /**
     * Sort holidays by duration.
     *
     * @return holidays sorted by duration.
     */
    public List<Holidays> sortByDuration() {
        return holidays.stream().sorted(new HolidaysDurationComparator()).collect(Collectors.toList());
    }

    /**
     * Find holidays by price.
     *
     * @param from the initial price limit
     * @param to   the ultimate price limit
     * @return holidays that fall into the price range
     */

    public List<Holidays> findByPrice(Double from, Double to) {
        return holidays.stream().filter(e -> e.getPrice() >= from && e.getPrice() <= to)
                .sorted(new HolidaysPriceComparator().thenComparing(new HolidaysDurationComparator())).collect(Collectors.toList());
    }

    /**
     * Find holidays by place.
     *
     * @param place boolean variable that returns true if the holiday goes outside
     * @return holidays that goes outside or inside
     */
    public List<Holidays> findByPlace(Boolean place) {
        return holidays.stream().filter(e -> e.isInFlat() == place).
                sorted(new HolidaysPriceComparator().thenComparing(new HolidaysDurationComparator())).collect(Collectors.toList());
    }

}

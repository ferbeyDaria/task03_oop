package com.epam;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

/**
 * Represents an view of application.
 *
 * @author Ferbey Daria
 * @version 1.6
 * @since 2019-11-10
 */
public class View {
    private Controller controller;
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private static Scanner input = new Scanner(System.in);


    public View() {
        controller = new Controller();
        menu = new LinkedHashMap<>();
        menu.put("1", "  1 - print all information");
        menu.put("2", "  2 - sorted by price");
        menu.put("3", "  3 - sorted by duration");
        menu.put("4", "  4 - find by place");
        menu.put("5", "  5 - find by price");
        menu.put("Q", "  Q - exit");

        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::pressButton1);
        methodsMenu.put("2", this::pressButton2);
        methodsMenu.put("3", this::pressButton3);
        methodsMenu.put("4", this::pressButton4);
        methodsMenu.put("5", this::pressButton5);
    }

    /**
     * Print information about all holidays.
     */
    private void pressButton1() {
        controller.printHolidays();
    }

    /**
     * Print holidays sorted by price.
     */
    private void pressButton2() {
        controller.printHolidaysSortedPrice();
    }

    /**
     * Print holidays sortd by duration.
     */
    private void pressButton3() {
        controller.printHolidaysSortedDuration();
    }

    /**
     * Find holidays that fall into price range.
     */
    private void pressButton4() {
        System.out.println("Please input whether the holiday will be indoors (true/false): ");
        boolean place = input.nextBoolean();
        controller.printHolidayPlace(place);
    }

    /**
     * Find holidays occurring either indoors or outdoors.
     */
    private void pressButton5() {
        System.out.println("Please input price limit: ");
        double from = input.nextDouble();
        double to = input.nextDouble();
        System.out.println("[" + from + ";" + to + "]");
        controller.printHolidayPrice(from, to);
    }

    //-----------------------------------------------------

    /**
     * Print menu.
     */
    private void outputMenu() {
        System.out.println("\nMENU:");
        for (String str : menu.values()) {
            System.out.println(str);
        }
    }

    /**
     * Print menu.
     */
    public void show() {
        String keyMenu;
        do {
            outputMenu();
            System.out.println("Please, select menu point.");
            keyMenu = input.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) {
            }
        } while (!keyMenu.equals("Q"));
    }

}

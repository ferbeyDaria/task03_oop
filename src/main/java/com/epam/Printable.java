package com.epam;

/**
 * Represents printable.
 *
 * @author Ferbey Daria
 * @version 1.6
 * @since 2019-11-10
 */
public interface Printable {
    void print();
}

package com.epam;

/**
 * Represents an Controller.
 *
 * @author Ferbey Daria
 * @version 1.6
 * @since 2019-11-10
 */
public class Controller {
    private BisnessLogical bl;

    public Controller() {
        bl = new BisnessLogical();
    }

    /**
     * Print informations about holidays.
     */
    public void printHolidays() {
        bl.showAll(bl.getHolidays());
    }

    /**
     * Print holidays sorted by price.
     */
    public void printHolidaysSortedPrice() {
        bl.showAll(bl.sortByPrice());
    }

    /**
     * Print holidays sorted by duration.
     */
    public void printHolidaysSortedDuration() {
        bl.showAll(bl.sortByDuration());
    }

    /**
     * Find holidays by place.
     *
     * @param place boolean variable that returns true if the holiday goes outside.
     */
    public void printHolidayPlace(Boolean place) {
        bl.showAll(bl.findByPlace(place));
    }

    /**
     * Find holidays by price range.
     *
     * @param from the initial price limit
     * @param to   the ultimate price limit
     */
    public void printHolidayPrice(Double from, Double to) {
        bl.showAll(bl.findByPrice(from, to));
    }

}

package com.epam;

import java.util.Comparator;
/**
 * Represents an comparator duration of holidays.
 *
 * @author Ferbey Daria
 * @version 1.6
 * @since 2019-11-10
 */
public class HolidaysDurationComparator implements Comparator<Holidays> {
    /**
     * Compare duration of holidays.
     * @param o1 first holiday
     * @param o2 second holiday
     * @return compare duration of holidays
     */
    @Override
    public int compare(Holidays o1, Holidays o2) {
        if (o1.getDuration()==o2.getDuration())
            return 0;
        return o1.getDuration()>o2.getDuration()?1:-1;
    }
}

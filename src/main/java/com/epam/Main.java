package com.epam;

/**
 * Represents an main.
 *
 * @author Ferbey Daria
 * @version 1.6
 * @since 2019-11-10
 */
public class Main {
    /**
     * This is method print menu and all method.
     *
     * @param args unused
     */
    public static void main(String[] args) {
        new View().show();
    }
}

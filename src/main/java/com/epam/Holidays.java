package com.epam;

/**
 * Represents an child`s holidays.
 *
 * @author Ferbey Daria
 * @version 1.6
 * @since 2019-11-10
 */
public class Holidays {
    private String nameHoliday;
    private boolean inFlat;
    private double price;
    private double duration;

    public Holidays(String nameHoliday, boolean inFlat, double price, double duration) {
        this.nameHoliday = nameHoliday;
        this.inFlat = inFlat;
        this.price = price;
        this.duration = duration;
    }

    /**
     * Gets name holidays.
     *
     * @return name holidays.
     */
    public String getNameHoliday() {
        return nameHoliday;
    }

    /**
     * Set name holiday.
     *
     * @param nameHoliday name holiday
     */
    public void setNameHoliday(String nameHoliday) {
        this.nameHoliday = nameHoliday;
    }

    /**
     * Get where to go for a holiday.
     *
     * @return boolean value
     */
    public boolean isInFlat() {
        return inFlat;
    }

    /**
     * Set where to go fo a holiday.
     *
     * @param inFlat whether the holiday is held indoors
     */
    public void setInFlat(boolean inFlat) {
        this.inFlat = inFlat;
    }

    /**
     * Get price of the holidays
     *
     * @return price of the holidays.
     */
    public double getPrice() {
        return price;
    }

    /**
     * Set price of the holiday.
     *
     * @param price price of the holiday
     */
    public void setPrice(double price) {
        this.price = price;
    }

    /**
     * Get duration of the holiday.
     *
     * @return duration of yhe holiday
     */
    public double getDuration() {
        return duration;
    }

    /**
     * Set duration of the holiday.
     *
     * @param duration duration ofthe holiday
     */
    public void setDuration(int duration) {
        this.duration = duration;
    }

}
